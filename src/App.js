import React, {Component} from 'react';
import './App.scss';

import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import Product from "./components/Product/Product";
import FileStorageApi from "./components/FileStorageApi/FileStorageApi";
import LocalStorageApi from "./components/LocalStorageApi/LocalStorageApi";


export default class App extends Component {

    fileStorageApi = new FileStorageApi();
    localStorageApi = new LocalStorageApi();

    state = {
        modal1: false,
        modal2: false,
        productsArray: [],
        favoriteArray: [],
        cartArray: [],
        currentProduct: null
    };

    componentDidMount() {
               this.fileStorageApi.getCard()
            .then((productsArray) => {
                this.setState({
                    productsArray: productsArray
                });
            });
    }

    approveActionFavorite = () =>{
        this.setState((state) => {
            const idx = state.favoriteArray.indexOf(state.currentProduct);

            if(idx<0) {
                return {
                    favoriteArray: [...state.favoriteArray, state.currentProduct],
                    modal1: false
                };
            }else{
                const favoriteArray = [
                    ...state.favoriteArray.slice(0, idx),
                    ...state.favoriteArray.slice(idx + 1)
                ];
                return {
                    favoriteArray: favoriteArray,
                    modal1: false
                }
            }
        });
        this.localStorageApi.putToLocalStorage('favorite_items', this.state.favoriteArray)
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.cartArray !== this.state.cartArray){
        this.localStorageApi.putToLocalStorage("cart_items", this.state.cartArray)
    }
        if (prevState.favoriteArray !== this.state.favoriteArray){
            this.localStorageApi.putToLocalStorage("favorite_items", this.state.favoriteArray)
        }
    }

    approveActionCart = () =>{
        this.setState((state) => {
            const idx = state.cartArray.indexOf(state.currentProduct);

            if(idx<0) {
                return {
                    cartArray: [...state.cartArray, state.currentProduct],
                    modal2: false
                };
            }else{
                const cartArray = [
                    ...state.cartArray.slice(0, idx),
                    ...state.cartArray.slice(idx + 1)
                ];
                return {
                    cartArray: cartArray,
                    modal2: false
                }
            }
        })
        this.localStorageApi.putToLocalStorage('cart_items', this.state.cartArray)
    };

    openFirstModal = (id) => {

        this.setState({modal1: true,
            currentProduct: id});

        this.localStorageApi.getFromLocalStorage()
            .then((productsArray) => {
                console.log('localStorageApi', productsArray);
            })
    };

    closeFirstModal = () => {
        this.setState({modal1: false});
    };


    openSecondModal = (id) => {
        this.setState({modal2: true,
            currentProduct: id});
    };

    closeSecondModal = () => {
        this.setState({modal2: false});
    };

    isFavorite = (id) => {
        console.log("this FAVOURITE",this.state.favoriteArray);
        const index = this.state.favoriteArray.indexOf(id);
        if (index < 0 ) return  false;

        return true;
    };
    isInCart = (id) => {
        console.log("cart Array", );
        const index = this.state.cartArray.indexOf(id);
        if (index < 0 ) return false;

        return true;
    };
    render() {

        const itemsArray = this.state.productsArray.map((product) => {
        const isFavorite = this.isFavorite(product.id);
        const isInCart = this.isInCart(product.id);

            const productActions = [
                (
                    <Button
                        key={"1"}
                        buttonClassName={"btn btn-primary"}
                        onClick={this.openFirstModal}
                        text={"Open Modal 1"}
                        backgroundColor={"pink"}
                        color={"blue"}
                        id={product.id}
                        type={"star"}
                        filled = {isFavorite}
                    />
                ),
                (
                    <Button
                        key={"2"}
                        buttonClassName={"btn btn-primary"}
                        onClick={this.openSecondModal}
                        text={"Open Modal 2"}
                        backgroundColor={"gray"}
                        id={product.id}
                        type={"cart"}
                        filled = {isInCart}
                    />
                )
            ];

            return (
                <Product
                    key={product.article}
                    product={product}
                    productActions={productActions}
                />
            );
        });


        const actions = [
            (<Button
                key={"3"}
                buttonClassName={"btn btn-primary"}
                onClick={this.approveActionFavorite}
                text={"Ok"}
                backgroundColor={"red"}
                color={"white"}
                type={"ok"}
            />),
            (<Button
                key={"4"}
                buttonClassName={"btn btn-secondary"}
                onClick={this.closeFirstModal}
                text={"Cancel"}
                backgroundColor={"red"}
                color={"white"}
                type={"cancel"}
            />)
        ];

        const actions2 = [
            (<Button
                key={"5"}
                buttonClassName={"btn btn-primary"}
                onClick={this.approveActionCart}
                text={"Ok"}
                backgroundColor={"red"}
                color={"white"}
                type={"ok"}
            />),
            (<Button
                key={"6"}
                buttonClassName={"btn btn-secondary"}
                onClick={this.closeSecondModal}
                text={"Cancel"}
                backgroundColor={"red"}
                color={"white"}
                type={"cancel"}
            />)
        ];

        return (
            <div className="App">

                {itemsArray}

                <Modal
                    key={1}
                    heder={"first"}
                    text={"Add this item to Favorites"}
                    actions={actions}
                    isOpen={this.state.modal1}
                    onClose={this.closeFirstModal}

                />

                <Modal
                    key={2}
                    heder={"Second"}
                    text={"Add this item to Shopping Cart"}
                    actions={actions2}
                    isOpen={this.state.modal2}
                    onClose={this.closeSecondModal}

                />
            </div>
        );
    }
    ;
}
