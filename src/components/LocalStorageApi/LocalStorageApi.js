import React, {Component} from 'react';
import Store from 'store';

class LocalStorageApi extends Component {

    getFromLocalStorage = async () =>{
        const res = await Store.get('dataArray');
        return res;
    }

    putToLocalStorage = (name, dataArray) =>{
        Store.set(name, dataArray);
    }
}


export default LocalStorageApi;